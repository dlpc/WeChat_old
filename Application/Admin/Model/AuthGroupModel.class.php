<?php
namespace Admin\Model;
use Think\Model;

class AuthGroupModel extends Model {
	protected $_validate = array(
			array('title','require','分组名称必须填写！'),
	);
	protected $_auto = array(
			array('create_time','time',1,'function'),
			array('update_time','time',2,'function'),
			array('status',1,1)
	);
	function lists() {
		$map['status'] = IS_ROOT?array('egt',-1):array('egt',0);
		return $this->where($map)->select();
	}
	function info($id) {
		$map['status'] = IS_ROOT?array('egt',-1):array('egt',0);
		return $this->where($map)->find($id);
	}
}