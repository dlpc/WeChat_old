<?php 
namespace Admin\Controller;

class ConfigController extends AdminController {
	
	function index() {
		$title = I('post.title');
		if (empty($title)) {
			$map = '';
		}else {
			$map['title'] = array('LIKE', "%{$title}%");
		}
		$list = $this->lists('Config', $map);
		$this->display();
	}
	
	function add() {
		if (IS_POST) {
			$Config = D('Config');
			if (!$Config->create()) {
				$this->err($Config->getError());
			}else {
				if ($Config->add()) {
					S('DB_CONFIG_DATA',null);
					$this->ok('新增配置成功！' ,'close');
				}else {
					$this->err('新增配置失败！');
				}
			}
		}else {
			$this->display('edit');
		}
	}
	
	function edit($id) {
		if (IS_POST) {
			$Config = D('Config');
			if (!$Config->create()) {
				$this->err($Config->getError());
			}else {
				if ($Config->save()) {
					S('DB_CONFIG_DATA',null);
					$this->ok('编辑配置成功！', 'close');
				}else {
					$this->err('编辑配置失败！');
				}
			}
		}else {
			$info = M('Config')->find($id);
			$this->assign('info', $info);
			$this->display('edit');
		}
	}
	
	function del() {
		$id = I('request.id');
		empty($id) && $this->err('至少选择一条数据！'.$id);
		$map['id'] = array('IN', $id);
		if (M('Config')->where($map)->delete()) {
			$this->ok('删除成功！');
		}else {
			$this->err('删除失败！'.$id);
		}
	}
	
	function group() {
		if (IS_POST) {
			$config = I('config');
			if($config && is_array($config)){
				$Config = M('Config');
				foreach ($config as $name => $value) {
					$map = array('name' => $name);
					$Config->where($map)->setField('value', $value);
				}
			}
			S('DB_CONFIG_DATA',null);
			$this->ok('保存成功！');
		}else {
			$lists = M('Config')->order('sort asc')->where('status=1')->select();
			$this->assign('_list',$lists);
			$this->display();
		}
	}
	
	function clear() {
		$cache = new \Think\Cache();
		if ($cache->connect()->clear()) {
			$this->ok('缓存清除成功！');
		}else {
			$this->err('缓存清除失败！');
		}
	}
}