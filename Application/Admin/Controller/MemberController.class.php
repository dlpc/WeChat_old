<?php
namespace Admin\Controller;

class MemberController extends AdminController {
	
	function index() {
		$nickname = I('nickname');
		$map = array();
		if(!empty($nickname)){
			$map['nickname']  = array('LIKE', "%{$nickname}%");
		}
		$this->lists('Member',$map,'last_login_time desc');
		$this->display();
	}
	
	function add() {
		if (IS_POST) {
			$model = D('Member');
			if (!$model->create()) {
				$this->err($model->getError());
			}else {
				if ($model->add()) {
					$this->ok('新增用户成功！');
				}else {
					$this->err('新增用户失败！');
				}
			}
		}else {
			$this->display('edit');
		}
	}
	
	protected function edit($id) {}
	
	function del() {
		if ($this->delete(D('Member'), I('id'))) {
			$this->success('删除用户成功！');
		}else {
			$this->error('删除用户失败！');
		}
	}
	
	function forb($id, $status) {
		if ($status == 1) {
			if ($this->forbidden(D('Member'), $id)) {
				$this->success('禁用用户成功！');
			}else {
				$this->error('禁用用户失败！');
			}
		}else {
			if ($this->restore(D('Member'), $id)) {
				$this->success('启用用户成功！');
			}else {
				$this->error('启用用户失败！');
			}
		}
	}

	function level() {
		$this->lists('MemberLevel');
		$this->display();
	}
	
	function level_add() {
		if (IS_POST) {
			$model = D('MemberLevel');
			if (!$model->create()) {
				$this->err($model->getError());
			}else {
				if ($model->add()) {
					$this->ok('添加成功！', 'close', 'Memberlevel');
				}else {
					$this->err('添加失败！');
				}
			}
		}else {
			$this->display('level_edit');
		}
	}
	
	function level_edit($id) {
		if (IS_POST) {
			$model = D('MemberLevel');
			if (!$model->create()) {
				$this->err($model->getError());
			}else {
				if ($model->save()) {
					$this->ok('编辑成功！', 'close', 'Memberlevel');
				}else {
					$this->err('编辑失败！');
				}
			}
		}else {
			$this->assign('info', M('MemberLevel')->find($id));
			$this->display('level_edit');
		}
	}
	
	function level_del($id) {
		if (M('Member')->where('level = '.$id)->count() > 0) {
			$this->err('该分组下有用户，不能直接删除！');
		}
		if (M('MemberLevel')->delete($id)) {
			$this->ok('删除成功！', '', 'Memberlevel');
		}else {
			$this->err('删除失败！');
		}
	}
}