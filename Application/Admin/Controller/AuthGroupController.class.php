<?php
/**
 * 用户组管理类
 * @package  Admin
 * @category AuthGroup
 * @author   C.Jango
 */
namespace Admin\Controller;

class AuthGroupController extends AdminController {
	
	function index() {
		$this->lists(D('AuthGroup'));
		$this->display();
	}
	
	function add() {
		if (IS_POST) {
			$model = D('AuthGroup');
			if (!$model->create()) {
				$this->err($model->getError());
			}else {
				if ($model->add()) {
					$this->ok('新增分组成功！');
				}else {
					$this->err('新增分组失败！');
				}
			}
		}else {
			$this->display('Auth/Group/edit');
		}
	}
	
	function edit($id) {
		if (IS_POST) {
			$model = D('AuthGroup');
			if (!$model->create()) {
				$this->error($model->getError());
			}else {
				if ($model->save()) {
					$this->success('编辑分组成功！',I('get.url',U('index')));
				}else {
					$this->error('编辑分组失败！');
				}
			}
		}else {
			$info = D('AuthGroup')->info($id);
			$this->assign('info',$info);
			$this->_meta_title('编辑分组');
			$this->display('Auth/Group/edit');
		}
	}
	
	function del() {
		if ($this->delete(D('AuthGroup'), I('id'))) {
			S('DB_CONFIG_DATA',null);
			$this->success('分组删除成功！');
		}else {
			$this->error('分组删除失败！');
		}
	}
	function forb($id, $status) {
		if ($status==1) {
			if ($this->forbidden(D('AuthGroup'), $id)){
				S('DB_CONFIG_DATA',null);
				$this->success('分组禁用成功！');
			}else {
				$this->error('分组禁用失败！');
			}
		}else {
			if ($this->restore(D('AuthGroup'), $id)){
				S('DB_CONFIG_DATA',null);
				$this->success('分组启用成功！');
			}else {
				$this->error('分组启用失败！');
			}
		}
	}
}