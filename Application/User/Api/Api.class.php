<?php
namespace User\Api;

abstract class Api {
	
	/**
	 * API调用模型实例
	 * @access protected
	 * @var object
	 */
	protected $model;
	
	/**
	 * 构造方法
	 */
	public function __construct() {
		$this->_init();
	}
	
	/**
	 * 抽象方法，用于设置模型实例
	 */
	abstract protected function _init();
}
