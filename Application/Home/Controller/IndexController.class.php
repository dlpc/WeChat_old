<?php
namespace Home\Controller;

class IndexController extends HomeController {

	function index() {
		$this->display();
	}
	
	function page($action='') {
		$this->assign('title', $action);
		$this->display();
	}
}