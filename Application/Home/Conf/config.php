<?php
/**
 * C.Jango Home模块配置文件
 * @author 、小陈叔叔 <www.cjango.com>
 */
return array(
	/* SESSION 和 COOKIE 配置 */
	'SESSION_PREFIX' => 'cjango_home',  //session前缀
	'COOKIE_PREFIX'  => 'cjango_home_', // Cookie前缀 避免冲突

	/* 模板相关配置 */
	'TMPL_PARSE_STRING' => array(
			'__STATIC__' => __ROOT__ . '/Public/Static',
			'__ADDONS__' => __ROOT__ . '/Public/' . MODULE_NAME . '/Addons',
			'__IMG__'    => __ROOT__ . '/Public/' . MODULE_NAME . '/images',
			'__CSS__'    => __ROOT__ . '/Public/' . MODULE_NAME . '/css',
			'__JS__'     => __ROOT__ . '/Public/' . MODULE_NAME . '/js',
			'__UPLOAD__' => __ROOT__ . '/Uploads',
	),
);