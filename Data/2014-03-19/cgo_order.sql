-- -----------------------------
-- Table structure for `cgo_order`
-- Backup date 2014-03-19 10:13:30
-- -----------------------------
DROP TABLE IF EXISTS `cgo_order`;
CREATE TABLE IF NOT EXISTS `cgo_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL,
  `total` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

