-- -----------------------------
-- Table structure for `cgo_member_level`
-- Backup date 2014-03-19 10:13:30
-- -----------------------------
DROP TABLE IF EXISTS `cgo_member_level`;
CREATE TABLE IF NOT EXISTS `cgo_member_level` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(30) NOT NULL,
  `cover_id` int(10) unsigned NOT NULL,
  `discount` decimal(5,2) unsigned NOT NULL,
  `create_time` int(10) unsigned NOT NULL,
  `update_time` int(10) unsigned NOT NULL,
  `remark` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- -----------------------------
-- Records of `cgo_member_level` Rows:7
-- -----------------------------
INSERT INTO `cgo_member_level` VALUES ('1', '初级会员', '0', '100.00', '1395061971', '1395195113', ''),
('2', '普通VIP', '0', '99.00', '1395061971', '1395195124', ''),
('3', '白银VIP', '0', '98.00', '1395195078', '0', ''),
('4', '黄金VIP', '0', '97.00', '1395195088', '0', ''),
('5', '白金VIP', '0', '96.00', '1395195097', '0', ''),
('6', '钻石VIP', '0', '95.00', '1395195104', '0', ''),
('7', '超级VIP', '0', '90.00', '1395195134', '0', '');