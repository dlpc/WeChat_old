-- -----------------------------
-- Table structure for `cgo_member`
-- Backup date 2014-03-18 15:32:36
-- -----------------------------
DROP TABLE IF EXISTS `cgo_member`;
CREATE TABLE IF NOT EXISTS `cgo_member` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `level` smallint(5) unsigned NOT NULL,
  `nickname` char(16) NOT NULL DEFAULT '' COMMENT '昵称',
  `headerpic` int(10) unsigned NOT NULL COMMENT '头像',
  `sex` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '性别',
  `birthday` date NOT NULL DEFAULT '0000-00-00' COMMENT '生日',
  `qq` char(10) NOT NULL DEFAULT '' COMMENT 'qq号',
  `score` mediumint(8) NOT NULL DEFAULT '0' COMMENT '用户积分',
  `login` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '登录次数',
  `reg_ip` bigint(20) NOT NULL DEFAULT '0' COMMENT '注册IP',
  `reg_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '注册时间',
  `last_login_ip` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后登录IP',
  `last_login_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最后登录时间',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '会员状态',
  PRIMARY KEY (`uid`),
  KEY `status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='会员表';

-- -----------------------------
-- Records of `cgo_member` Rows:2
-- -----------------------------
INSERT INTO `cgo_member` VALUES ('1', '0', '超级管理', '0', '0', '0000-00-00', '', '0', '7', '0', '1394761178', '0', '1395062520', '1'),
('2', '0', 'root', '0', '0', '0000-00-00', '', '0', '1', '0', '0', '0', '1394865248', '1');