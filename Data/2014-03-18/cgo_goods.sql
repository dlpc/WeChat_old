-- -----------------------------
-- Table structure for `cgo_goods`
-- Backup date 2014-03-18 15:32:36
-- -----------------------------
DROP TABLE IF EXISTS `cgo_goods`;
CREATE TABLE IF NOT EXISTS `cgo_goods` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cate_id` int(10) unsigned NOT NULL,
  `title` int(120) NOT NULL,
  `keywords` varchar(255) NOT NULL,
  `view` int(10) unsigned NOT NULL,
  `market_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `promote_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `content` text NOT NULL,
  `create_time` int(10) unsigned NOT NULL,
  `update_time` int(10) unsigned NOT NULL,
  `amount` int(10) unsigned NOT NULL,
  `selled` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

