-- -----------------------------
-- Table structure for `cgo_reply_attention`
-- Backup date 2014-03-18 15:32:36
-- -----------------------------
DROP TABLE IF EXISTS `cgo_reply_attention`;
CREATE TABLE IF NOT EXISTS `cgo_reply_attention` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `create_time` int(10) unsigned NOT NULL,
  `update_time` int(10) unsigned NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- -----------------------------
-- Records of `cgo_reply_attention` Rows:1
-- -----------------------------
INSERT INTO `cgo_reply_attention` VALUES ('1', '1393382885', '1393736979', '欢迎关注微信分享平台，我们暂时为您提供了以下功能：\r\n1:天气查询\r\n2:笑话\r\n直接回复数字，即可显示您要查看的内容');