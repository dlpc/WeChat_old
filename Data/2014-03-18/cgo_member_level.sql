-- -----------------------------
-- Table structure for `cgo_member_level`
-- Backup date 2014-03-18 15:32:36
-- -----------------------------
DROP TABLE IF EXISTS `cgo_member_level`;
CREATE TABLE IF NOT EXISTS `cgo_member_level` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(30) NOT NULL,
  `cover_id` int(10) unsigned NOT NULL,
  `discount` decimal(5,2) unsigned NOT NULL,
  `create_time` int(10) unsigned NOT NULL,
  `update_time` int(10) unsigned NOT NULL,
  `remark` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- -----------------------------
-- Records of `cgo_member_level` Rows:2
-- -----------------------------
INSERT INTO `cgo_member_level` VALUES ('1', '初级会员', '0', '100.00', '1395061971', '0', ''),
('2', '普通VIP', '0', '99.00', '1395061971', '0', '');